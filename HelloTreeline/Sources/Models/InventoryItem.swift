//
//  InventoryItem.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

struct InventoryItem: Codable {
    let id: String
    let type: TypeEnum
    let color: String
    var available: Int
    let title: String
    let description: String
}

enum TypeEnum: String, Codable {
    case shirt = "Shirt"
}
