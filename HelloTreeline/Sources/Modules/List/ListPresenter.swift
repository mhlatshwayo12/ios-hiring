//
//  ListPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class ListPresenter: ListInteractorDelegate {

    private let router: ListRouter
    private let interactor: ListInteractor
    private weak var view: ListViewController!
    
    private var listItems: [ListItem] = []
    
    var itemCount: Int { listItems.count }

    init(view: ListViewController, interactor: ListInteractor, router: ListRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router

        self.interactor.delegate = self
    }
    
    func viewWillAppear() {
        interactor.loadItems()
    }
    
    func listItem(at indexPath: IndexPath) -> ListItem {
        listItems[indexPath.row]
    }
    
    func listItemSelected(objectId: String) {
        router.routeToDetails(objectId: objectId)
    }
    
    func itemsDidLoad(items: [InventoryItem]) {
        self.listItems = items.map { .init(id: $0.id, title: $0.title) }
        self.view.reloadList()
    }
    
    func presentError(error: String) {
        self.view.presentError(error: error)
    }
}
