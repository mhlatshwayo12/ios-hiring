//
//  ListViewContoller.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import MessageUI
import UIKit

protocol ListViewController: AnyObject {
    func reloadList()
    func presentError(error: String)
}

class ListDefaultViewController: UIViewController, ListViewController, MFMailComposeViewControllerDelegate {

    static func build() -> ListDefaultViewController {
        let viewController = UIStoryboard.main.instantiateViewController(of: ListDefaultViewController.self)!
        let router = ListDefaultRouter(viewController: viewController)
        let interactor = ListInteractor()

        viewController.presenter = ListPresenter(view: viewController, interactor: interactor, router: router)

        return viewController
    }

    private var presenter: ListPresenter!

    @IBOutlet weak var tableView: UITableView!
    
    private let CellIdentifier = "ListCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear()
    }
    
    @IBAction func submitSalesButtonPressed(_ sender: Any) {
        submitSales()
    }
    
    func reloadList() {
        tableView.reloadData()
    }
    
    func presentError(error: String) {
        let alert = UIAlertController(title: "Sorry, there was an issue.", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func submitSales() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["bossman@bosscompany.com"])
            mail.setSubject("Sales Today")
            mail.setMessageBody("Here are the items we sold today!", isHTML: false)

            present(mail, animated: true)
        } else {
            if let mailURLString = "mailto:bossman@bosscompany.com?subject=Sales Today".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
               let mailURL = URL(string: mailURLString) {
                if UIApplication.shared.canOpenURL(mailURL) {
                    view.window?.windowScene?.open(mailURL, options: nil, completionHandler: nil)
                } else {
                    presentError(error: "We could not send the email")
                }
            }
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension ListDefaultViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)!
        cell.textLabel?.text = presenter.listItem(at: indexPath).title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objectId = presenter.listItem(at: indexPath).id
        presenter.listItemSelected(objectId: objectId)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
