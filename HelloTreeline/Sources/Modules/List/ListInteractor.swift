//
//  ListInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol ListInteractorDelegate: AnyObject {
    func itemsDidLoad(items: [InventoryItem])
    func presentError(error: String)
}

class ListInteractor {

    weak var delegate: ListInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository

    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.itemRepository = itemRepository
    }
    
    func loadItems() {
        itemRepository.getAll { result in
            switch result {
            case .success(let remoteItems):
                self.delegate?.itemsDidLoad(items: remoteItems)
            case .failure(let error):
                self.delegate?.presentError(error: error.localizedDescription)
                break
            }
        }
    }
}
