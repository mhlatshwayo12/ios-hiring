//
//  DetailsPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class DetailsPresenter: DetailsInteractorDelegate {

    private let router: DetailsRouter
    private let interactor: DetailsInteractor
    private weak var view: DetailsViewController!
    
    private let objectId: String

    init(view: DetailsViewController,
         interactor: DetailsInteractor,
         router: DetailsRouter,
         objectId: String) {
        
        self.view = view
        self.interactor = interactor
        self.router = router

        self.objectId = objectId
        
        self.interactor.delegate = self
    }
    
    func viewDidLoad() {
        interactor.loadItem(objectId: objectId)
    }
    
    func dismissDetailViewAfterTransaction() {
        view.dismissView()
    }
    
    func presentError(error: String) {
        view.presentError(error: error)
    }
    
    func itemDidLoad(item: InventoryItem) {
        view.navBarTitle = item.title
        view.idLabelTitle = "ID: \(item.id)"
        view.titleLabelTitle = "Title: \(item.title)"
        view.desciptionLabelTitle = "Description: \(item.description)"
        view.colorLabelTitle = "Color: \(item.color)"
        view.quantityLabelTitle = "Quantity: \(item.available)"
    }
    
    func itemWasSold() {
        interactor.itemWasSold(objectId: objectId)
    }
    
    func itemWasReturned() {
        interactor.itemWasReturned(objectId: objectId)
    }
}
