//
//  DetailsInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol DetailsInteractorDelegate: AnyObject {
    func itemDidLoad(item: InventoryItem)
    func itemWasSold()
    func itemWasReturned()
    func dismissDetailViewAfterTransaction()
    func presentError(error: String)
}

class DetailsInteractor {

    weak var delegate: DetailsInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository

    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.itemRepository = itemRepository
    }
    
    func loadItem(objectId: String) {
        guard let item = itemRepository.get(objectId: objectId) else { return }
        
        delegate?.itemDidLoad(item: item)
    }
    
    func itemWasSold(objectId: String) {
        itemRepository.saveItemTransaction(objectId: objectId, interaction: .itemSale) { [weak self] error in
            if error != nil {
                self?.delegate?.presentError(error: error ?? InventoryTransactionError.unknownError.rawValue)
            }
            else {
                self?.delegate?.dismissDetailViewAfterTransaction()
            }
        }
    }
    
    func itemWasReturned(objectId: String) {
        itemRepository.saveItemTransaction(objectId: objectId, interaction: .itemReturn) { [weak self] error in
            if error != nil {
                self?.delegate?.presentError(error: error ?? InventoryTransactionError.unknownError.rawValue)
            }
            else {
                self?.delegate?.dismissDetailViewAfterTransaction()
            }
        }
    }
}
