//
//  InventoryItemRepository.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol InventoryItemRepository {
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void)
    func get(objectId: String) -> InventoryItem?
    func saveItemTransaction(objectId: String, interaction: InteractionType, completion: @escaping (String?) -> Void)
}

enum InteractionType {
    case itemSale
    case itemReturn
}

enum InventoryTransactionError: String, Error {
    case unknownItem = "We could not find this item"
    case unknownError = "There was an issue with this transaction"
}

class DefaultInventoryItemRepository: InventoryItemRepository {
    
    static let shared: InventoryItemRepository = DefaultInventoryItemRepository()
    
    private let apiClient: APIClient
    
    private var items: [InventoryItem] = []
    
    init(apiClient: APIClient = DefaultAPIClient.shared) {
        self.apiClient = apiClient
    }
    
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void) {
        apiClient.get(endpoint: "getInventory", type: [InventoryItem].self) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let items):
                    self.items = items
                    completion(.success(self.items))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func get(objectId: String) -> InventoryItem? {
        guard let item = items.first(where: { $0.id == objectId }) else { return nil }
            
        if let savedInventoryItem = UserDefaults.standard.object(forKey: "\(item.id)") as? Data {
            let decoder = JSONDecoder()
            if let loadedItem = try? decoder.decode(InventoryItem.self, from: savedInventoryItem) {
                return loadedItem
            }
        }
        
        return items.first(where: { $0.id == objectId })
    }
    
    func saveItemTransaction(objectId: String, interaction: InteractionType, completion: @escaping (String?) -> Void) {
        
        guard var item = get(objectId: objectId) else {
            completion(InventoryTransactionError.unknownItem.rawValue)
            return
        }

        switch interaction {
        case .itemSale:
            let encoder = JSONEncoder()
            if let currentQuantity = getCurrentNumberSold(objectId: objectId)  {
                item.available = currentQuantity - 1
                let encoded = try? encoder.encode(item)
                UserDefaults.standard.set(encoded, forKey: "\(item.id)")
            }
            else {
                item.available -= 1
                let encoded = try? encoder.encode(item)
                UserDefaults.standard.set(encoded, forKey: "\(item.id)")
            }
                
        case .itemReturn:
            let encoder = JSONEncoder()
            if let currentQuantity = getCurrentNumberSold(objectId: objectId)  {
                item.available = currentQuantity + 1
                let encoded = try? encoder.encode(item)
                UserDefaults.standard.set(encoded, forKey: "\(item.id)")
            }
            else {
                item.available += 1
                let encoded = try? encoder.encode(item)
                UserDefaults.standard.set(encoded, forKey: "\(item.id)")
            }
        }
        completion(nil)
    }
    
    func getCurrentNumberSold(objectId: String) -> Int? {
        if let item = get(objectId: objectId), let savedInteraction = UserDefaults.standard.object(forKey: "\(item.id)") as? Data {
            if let loadedItem = try? JSONDecoder().decode(InventoryItem.self, from: savedInteraction) {
                return loadedItem.available
            }
        }
        return nil
    }
}
