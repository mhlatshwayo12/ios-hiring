//
//  ListPresenterTests.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import XCTest
@testable import HelloTreeline

class ListPresenterTests: XCTestCase {
    
    private var presenter: ListPresenter!
    private var mockRouter: MockRouter!
    private var view: MockViewController!
    
    private let inventoryItems = [
        InventoryItem(id: "test-id-1", type: .shirt, color: "color", available: 1001, title: "title", description: "description-1"),
        InventoryItem(id: "test-id-2", type: .shirt, color: "color", available: 1002, title: "title", description: "description-2"),
        InventoryItem(id: "test-id", type: .shirt, color: "color", available: 1003, title: "title", description: "description-3")
    ]

    override func setUpWithError() throws {
        
        self.mockRouter = MockRouter()
        self.view = MockViewController()
        
        
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = inventoryItems
        let interactor = ListInteractor(itemRepository: itemRepository)
        
        self.presenter = ListPresenter(view: view, interactor: interactor, router: mockRouter)
    }
    
    func testViewWillAppearToLoadListItems() {
        
        // before
        XCTAssertEqual(presenter.itemCount, 0)
        XCTAssertEqual(view.reloadListCalledCount, 0)
        
        // when
        presenter.viewWillAppear()
        
        // then
        XCTAssertEqual(presenter.itemCount, inventoryItems.count)
        XCTAssertEqual(view.reloadListCalledCount, 1)
        for i in 0..<(inventoryItems.count) {
            let listItem = presenter.listItem(at: IndexPath(row: i, section: 0))
            let inventoryItem = inventoryItems[i]
            XCTAssertEqual(listItem.id, inventoryItem.id)
            XCTAssertEqual(listItem.title, inventoryItem.title)
        }
    }
    
    func testListItemSelection() {
        
        // before
        XCTAssertNil(mockRouter.routeToDetailsCallObjectId)
        
        // when
        presenter.viewWillAppear()
        
        let objectId = inventoryItems[0].id
        presenter.listItemSelected(objectId: objectId)
        
        // then
        XCTAssertEqual(mockRouter.routeToDetailsCallObjectId, objectId)
    }
}

fileprivate class MockViewController: ListViewController {
    func presentError(error: String) {}
    
    
    var reloadListCalledCount = 0
    func reloadList() {
        reloadListCalledCount += 1
    }
}

fileprivate class MockRouter: ListRouter {
    
    var routeToDetailsCallObjectId: String? = nil
    func routeToDetails(objectId: String) {
        routeToDetailsCallObjectId = objectId
    }
}
